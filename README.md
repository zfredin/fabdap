# fabDAP

SAMD11C-based CMSIS-DAP based on https://github.com/ataradov/free-dap and Neil's demo board from MAS.863

Tested good on an old SAMD51 board using edbg. Left the power switch and LED off since they don't quite fit. 

![fabDAP_assembled](fabDAP_assembled.jpg)

Build programmers into everything!